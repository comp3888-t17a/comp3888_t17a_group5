import cv2
import sys
import numpy as np
from src import config as rmcfg
import src.cv.signs as detect
import os
from PIL import Image
from datetime import date

directory = "C:/Users/Patrick Delaney/Desktop/projects/gym-donkeycar/mysim/rmracerlib/test-data/real_life"
os.chdir(directory)

#park_green
os.chdir(directory)
test_files = "park_green"
os.chdir(test_files)
park_green_pos = 0
count = 0
for filename in os.listdir(os.getcwd()):
    #frame = Image.open(filename)
    frame = cv2.imread(filename)
    #hsv = cv2.cvtColor(np.float32(frame), rmcfg.COLOUR_CONVERT)
    hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)
    frame, result = detect.detect_park(frame,hsv)
    if result:
        park_green_pos += 1
        count += 1
    else:
        count += 1
success_rate_park = park_green_pos/count * 100
failure_rate_park = 100-success_rate_park

#left_turn
os.chdir(directory)
test_files = "left_turn"
os.chdir(test_files)
left_turn_pos = 0
count = 0

for filename in os.listdir(os.getcwd()):
    #frame = Image.open(filename)
    frame = cv2.imread(filename)
    #hsv = cv2.cvtColor(np.float32(frame), rmcfg.COLOUR_CONVERT)
    hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)
    frame, result = detect.detect_turn(frame,hsv)
    if result:
        left_turn_pos += 1
        count += 1
    else:
        count += 1
success_rate_left_turn = left_turn_pos/count * 100
failure_rate_left_turn = 100-success_rate_left_turn

#right_turn
os.chdir(directory)
test_files = "right_turn"
os.chdir(test_files)
right_turn_pos = 0
count = 0

for filename in os.listdir(os.getcwd()):
    #frame = Image.open(filename)
    frame = cv2.imread(filename)
    #hsv = cv2.cvtColor(np.float32(frame), rmcfg.COLOUR_CONVERT)
    hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)
    frame, result = detect.detect_turn(frame,hsv)
    if result:
        right_turn_pos += 1
        count += 1
    else:
        count += 1
success_rate_right_turn = right_turn_pos/count * 100
failure_rate_right_turn = 100-success_rate_right_turn

#stop
os.chdir(directory)
test_files = "stop"
os.chdir(test_files)
stop_pos = 0
count = 0

for filename in os.listdir(os.getcwd()):
    #frame = Image.open(filename)
    frame = cv2.imread(filename)
    #hsv = cv2.cvtColor(np.float32(frame), rmcfg.COLOUR_CONVERT)
    hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)
    frame, result = detect.detect_stop(frame,hsv)
    if result:
        stop_pos += 1
        count += 1
    else:
        count += 1
success_rate_stop = stop_pos/count * 100
failure_rate_stop = 100-success_rate_stop

#other
#os.chdir(directory)
#test_files = "other"
#os.chdir(test_files)

print("success of green park: " + str(success_rate_park))
print("success of left turn: " + str(success_rate_left_turn))
print("success of right turn: " + str(success_rate_right_turn))
print("success of stop: " + str(success_rate_stop))

#traffic
#os.chdir(directory)
#due to our traffic detection not working this has been commented out for the time being