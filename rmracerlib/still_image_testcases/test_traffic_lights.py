from src.cv import lights as lights
import cv2
import sys
from src import config as rmcfg


cv2.namedWindow("live")

while True:
  # Test 1
  frame = cv2.imread("test-data/lights_test/green_1.jpg")
  frame, result = lights.detect_traffic(frame)
  if result != 'green':
    print("Failed test 1 - expected green, got", result)
  else:
    print("Pass test 1")
  cv2.imshow("live", frame)

  key = cv2.waitKey(2000)
  if key == ord('q'):
    break

  # Test 2
  frame = cv2.imread("test-data/lights_test/amber_1.jpg")
  frame, result = lights.detect_traffic(frame)
  if result != 'amber':
    print("Failed test 2 - expected amber, got", result)
  else:
    print("Pass test 2")

  cv2.imshow("live", frame)

  key = cv2.waitKey(2000)
  if key == ord('q'):
    break

  # Test 3
  frame = cv2.imread("test-data/lights_test/red_1.jpg")
  frame, result = lights.detect_traffic(frame)
  if result != 'red':
    print("Failed test 2 - expected red, got", result)
  else:
    print("Pass test 2")

  cv2.imshow("live", frame)

  key = cv2.waitKey(2000)
  if key == ord('q'):
    break

cv2.destroyAllWindows()
sys.exit()
