from src.cv import signs as park
from src import config as rmcfg
import cv2
import sys
import numpy as np

img_path = 'test-data/park_test/park_green_1.jpg'
cv2.namedWindow("live")
kernel = np.ones((rmcfg.KERNEL_SIZE,rmcfg.KERNEL_SIZE), np.uint8)

while True:
    # Test 1
    frame = cv2.imread("test-data/park_test/park_green_1.jpg")
    hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)
    frame, result = park.detect_park(frame, hsv)
    lower_green_park = np.array([50, 55, 55])   #green filter
    upper_green_park = np.array([90, 255, 230])
    #lower_white = np.array([90,10,125])
    #upper_white = np.array([133,67,161])
    #mask = cv2.inRange(hsv, lower_white, upper_white)
    mask = cv2.inRange(hsv, lower_green_park, upper_green_park)
    mask1 = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel,iterations=2)
    mask2 = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel,iterations=1)
    height, width = mask.shape[:2]
    rmcfg.ROOF = int(height*rmcfg.ROOF_R)
    rmcfg.HORIZON = int(height*rmcfg.HORIZON_R)

    contours, _ = cv2.findContours(mask[0:rmcfg.HORIZON, 0:width], cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    result = False
    cv2.drawContours(frame, contours, -1, (255,0,0), 1)
    if not result:
      print("Failed test 1 - false negative")
    else:
      print("Pass test 1")
    cv2.imshow("live", frame)
    cv2.imshow("mask", mask)
    cv2.imshow("mask close", mask1)
    cv2.imshow("mask open", mask2)

    key = cv2.waitKey(20000)
    if key == ord('q'):
      break

    # Test 1
    frame = cv2.imread("test-data/park_test/white_park_1.jpg")
    hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)
    frame, result = park.detect_park_white(frame, hsv)
    lower_white = np.array([90,10,125])
    upper_white = np.array([133,67,161])
    mask = cv2.inRange(hsv, lower_white, upper_white)
    mask1 = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel,iterations=2)
    mask2 = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel,iterations=1)
    height, width = mask.shape[:2]
    rmcfg.ROOF = int(height*rmcfg.ROOF_R)
    rmcfg.HORIZON = int(height*rmcfg.HORIZON_R)

    contours, _ = cv2.findContours(mask[0:rmcfg.HORIZON, 0:width], cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    result = False
    cv2.drawContours(frame, contours, -1, (255,0,0), 1)
    if not result:
      print("Failed test 1 - false negative")
    else:
      print("Pass test 1")
    cv2.imshow("live", frame)
    cv2.imshow("mask", mask)
    cv2.imshow("mask close", mask1)
    cv2.imshow("mask open", mask2)

    key = cv2.waitKey(20000)
    if key == ord('q'):
      break

    # # Test 2
    # frame = cv2.imread("test-data/park_test/park_green_2.jpg")
    # hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)
    # frame, result = park.detect_park(frame, hsv)
    # mask = cv2.inRange(hsv, lower_green_park, upper_green_park)
    # mask1 = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel,iterations=2)
    # mask2 = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel,iterations=1)
    # if not result:
    #   print("Failed test 1 - false negative")
    # else:
    #   print("Pass test 1")
    # cv2.imshow("live", frame)
    # cv2.imshow("mask", mask)
    # cv2.imshow("mask close", mask1)
    # cv2.imshow("mask open", mask2)
    #
    # key = cv2.waitKey(2000)
    # if key == ord('q'):
    #   break


cv2.destroyAllWindows()
sys.exit()
