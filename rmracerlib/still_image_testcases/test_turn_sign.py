import src.cv.signs as turn
import cv2
import sys
from src import config as rmcfg



cv2.namedWindow("live")


img_path = "test-data/turn_test/turn_v1_right_1.jpg"

if (len(sys.argv) > 1):
    if (sys.argv[1] == 't'):
        def nothing(x):
            pass
        cv2.namedWindow("trackbars")
        cv2.createTrackbar("L-H", "trackbars", 160, 180, nothing)
        cv2.createTrackbar("L-S", "trackbars", 75, 255, nothing)
        cv2.createTrackbar("L-V", "trackbars", 50, 255, nothing)
        cv2.createTrackbar("U-H", "trackbars", 180, 180, nothing)
        cv2.createTrackbar("U-S", "trackbars", 255, 255, nothing)
        cv2.createTrackbar("U-V", "trackbars", 255, 255, nothing)
        def create_trackbars(frame):
            # convert to HSV image
            hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)

            l_h = cv2.getTrackbarPos("L-H", "trackbars")
            l_s = cv2.getTrackbarPos("L-S", "trackbars")
            l_v = cv2.getTrackbarPos("L-V", "trackbars")
            u_h = cv2.getTrackbarPos("U-H", "trackbars")
            u_s = cv2.getTrackbarPos("U-S", "trackbars")
            u_v = cv2.getTrackbarPos("U-V", "trackbars")

            # l_h2 = cv2.getTrackbarPos("L-H", "trackbars2")
            # l_s2 = cv2.getTrackbarPos("L-S", "trackbars2")
            # l_v2 = cv2.getTrackbarPos("L-V", "trackbars2")
            # u_h2 = cv2.getTrackbarPos("U-H", "trackbars2")
            # u_s2 = cv2.getTrackbarPos("U-S", "trackbars2")
            # u_v2 = cv2.getTrackbarPos("U-V", "trackbars2")

            lower_trackbars = np.array([l_h, l_s, l_v])
            upper_trackbars = np.array([u_h, u_s, u_v])

            # lower_trackbars2 = np.array([l_h, l_s, l_v])
            # upper_trackbars2 = np.array([u_s, u_h, u_v])

            mask_tbars1 = cv2.inRange(hsv, lower_trackbars, upper_trackbars)
            # mask_tbars2 = cv2.inRange(hsv, lower_trackbars2, upper_trackbars2)
            mask_tbars = mask_tbars1 # + mask_tbars2
            kernel = np.ones((5,5), np.uint8)
            # using morphology to test for end result
            mask_tbars = cv2.morphologyEx(mask_tbars, cv2.MORPH_OPEN, kernel, iterations=1)
            mask_tbars = cv2.morphologyEx(mask_tbars, cv2.MORPH_CLOSE, kernel, iterations=1)
            mask_tbars = cv2.dilate(mask_tbars, kernel, iterations=2)

            trackview = cv2.bitwise_and(frame,frame,mask = mask_tbars)
            cv2.imshow("trackbarview", trackview)

        while True:
          frame = cv2.imread(img_path)
          create_trackbars(frame)
          hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)
          frame, result = turn.detect_turn(frame,hsv)
          cv2.imshow("live", frame)
          key = cv2.waitKey(50)
          if key == ord('q'):
            break
    elif (sys.argv[1] == 'd'):
      while True:
        frame = cv2.imread(img_path)
        hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)
        frame, result = turn.detect_turn(frame,hsv)
        cv2.imshow("live",frame)
        key = cv2.waitKey(50)
        if key == ord('q'):
          break
else:
  while True:
    # Test 1
    frame = cv2.imread("test-data/turn_test/turn_false_positive.jpg")
    hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)
    frame, result = turn.detect_turn(frame, hsv)
    if result is not None:
      print("Failed test 1 - false positive")
    else:
      print("Pass test 1")
    cv2.imshow("live", frame)

    key = cv2.waitKey(2000)
    if key == ord('q'):
      break

    # Test 2
    frame = cv2.imread("test-data/turn_test/turn_v1_left_1.jpg")
    hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)
    frame, result = turn.detect_turn(frame, hsv)
    if result is None:
      print("Failed test 2 - didn't detect sign")
    elif result == 'right':
      print("Failed test 2 - wrong direction")
    else:
      print("Pass test 2")

    cv2.imshow("live", frame)

    key = cv2.waitKey(2000)
    if key == ord('q'):
      break


    # Test 3
    frame = cv2.imread("test-data/turn_test/turn_v1_left_2.jpg")
    hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)
    frame, result = turn.detect_turn(frame, hsv)

    if result is None:
      print("Failed test 3 - didn't detect sign")
    elif result == 'right':
      print("Failed test 3 - wrong direction")
    else:
      print("Pass test 3")

    cv2.imshow("live", frame)

    key = cv2.waitKey(2000)
    if key == ord('q'):
      break

    # Test 4
    frame = cv2.imread("test-data/turn_test/turn_v1_left_blue_bg_1.jpg")
    hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)
    frame, result = turn.detect_turn(frame, hsv)
    if result is None:
      print("Failed test 4 - didn't detect sign")
    elif result == 'right':
      print("Failed test 4 - wrong direction")
    else:
      print("Pass test 4")

    cv2.imshow("live", frame)

    key = cv2.waitKey(2000)
    if key == ord('q'):
      break

    # Test 5
    frame = cv2.imread("test-data/turn_test/turn_v1_left_blue_bg_2.jpg")
    hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)
    frame, result = turn.detect_turn(frame, hsv)
    if result is None:
      print("Failed test 5 - didn't detect sign")
    elif result == 'right':
      print("Failed test 5 - wrong direction")
    else:
      print("Pass test 5")

    cv2.imshow("live", frame)

    key = cv2.waitKey(2000)
    if key == ord('q'):
      break


cv2.destroyAllWindows()
sys.exit()
