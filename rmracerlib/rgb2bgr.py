import cv2

frame = cv2.imread("test-data/stop_2.jpg")
img = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
cv2.imwrite("test-data/stop_2_bgr.jpg", img)