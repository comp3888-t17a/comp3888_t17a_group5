import cv2
import sys
import numpy as np
from src import config as rmcfg
import src.cv.signs as stop
import os
from PIL import Image
from datetime import date

#cv2.namedWindow("live")

#Logical statements to determine what sign specifically you want to test
while True:
    which_shape = input("Please identify which sign you want to test - 'Circle' or 'Arrow': ").lower()
    if which_shape == "circle":
        shape = "Circle"
        break
    elif which_shape == "arrow":
        shape = "Arrow"
        break
    else:
        print("ERROR: Please enter in one of the two options given")

while True:
    which_colour = input("Please identify which colour you want to test - 'Blue' or 'Black: ").lower()
    if which_colour == "black":
        colour = "Black"
        break
    elif which_colour == "blue":
        colour = "Blue"
        break
    else:
        print("ERROR: Please enter in one of the two options given")

while True:
    which_direction = input("Please identify which turn direction you want to test - 'Left' or 'Right': ").lower()
    if which_direction == "left":
        direction = "Left"
        break
    elif which_direction == "right":
        direction = "Right"
        break
    else:
        print("ERROR: Please enter in one of the two options given")

if shape == "Circle":
    if colour == "Black":
        if direction == "Left":
            directory = "test-data/efficiency_testing/black_circle_left"
        else:
            directory = "test-data/efficiency_testing/black_circle_right"
    else:
        if direction == "Left":
            directory = "test-data/efficiency_testing/blue_circle_left"
        else:
            directory = "test-data/efficiency_testing/blue_circle_right"
else:
    if colour == "Black":
        if direction == "Left":
            directory = "test-data/efficiency_testing/black_arrow_left"
        else:
            directory = "test-data/efficiency_testing/black_arrow_right"
    else:
        if direction == "Left":
            directory = "test-data/efficiency_testing/blue_arrow_left"
        else:
            directory = "test-data/efficiency_testing/blue_arrow_right"
os.chdir(directory) 

count = 0 
correct = 0
false_positive = 0
false_negative = 0
incorrect_direction = 0
for filename in os.listdir(os.getcwd()):
    print(filename)
    #frame = Image.open(filename)
    frame = cv2.imread(filename)
    #hsv = cv2.cvtColor(np.float32(frame), rmcfg.COLOUR_CONVERT)
    hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)
    frame, result = stop.detect_turn(frame,hsv)
    cv2.imshow("live", frame)
    cv2.waitKey(1)
    while True:
        txt = input("-----------\nEnter 'q' for box present in the right spot;\nEnter 'w' for box present but wrong spot;\nEnter 'e' for box present in correct spot and incorrect spot (more than 1 box);\nEnter 'r' for no box present where it should be;\nEnter 't' if no box is present at all and there shouldn't be one;\nEnter 'y' if box is present but the sign direction is incorrect: ").lower()
        if txt == "q":
            count += 1
            correct += 1
            break
        elif txt == "w":
            count += 1
            false_positive += 1
            break
        elif txt == "e":
            count += 1
            false_positive += 1
            correct += 1
            break
        elif txt == "r":
            count += 1
            false_negative += 1
            break
        elif txt == "t":
            #count += 1
            break
        elif txt == "y":
            count += 1
            incorrect_direction += 1
            break
        else:
            print("-----------\nERROR: Incorrect input, please try again")
correct_rate = round((correct/count * 100),2)
false_positive_rate = round((false_positive/count * 100),2)
false_negative_rate = round((false_negative/count * 100),2)
incorrect_direction_rate = round((incorrect_direction/count * 100),2)
results = ("Correct Accuracy %: " + str(correct_rate) + "\nFalse Positive %: " + str(false_positive_rate) + "\nFalse Negative %: " + str(false_negative_rate) + "\nIncorrect Direction %" + str(incorrect_direction_rate))
print(results)
user = input("Please enter your name for logging purposes: ")
current_date = date.today()
formatted_date = current_date.strftime("%B %d, %Y")
input_text = ("---These results for " + str(colour) + " " + str(shape) + " " + str(direction) +  " turn sign were recorded on " + str(formatted_date) + " by " + user + "---\n" + results + "\n\n")

#Logical statements to determine which file to write in
if shape == "Circle":
    if colour == "Black":
        if direction == "Left":
            reportfile = "circle_black_left_turn_efficiency.txt"
        else:
            reportfile = "circle_black_right_turn_efficiency.txt"
    else:
        if direction == "Left":
            reportfile = "circle_blue_left_turn_efficiency.txt"
        else:
            reportfile = "circle_blue_right_turn_efficiency.txt"
else:
    if colour == "Black":
        if direction == "Left":
            reportfile = "arrow_black_left_turn_efficiency.txt"
        else:
            reportfile = "arrow_black_right_turn_efficiency.txt"
    else:
        if direction == "Left":
            reportfile = "arrow_blue_left_turn_efficiency.txt"
        else:
            reportfile = "arrow_blue_right_turn_efficiency.txt"

os.chdir("C:/Users/Patrick Delaney/Desktop/projects/gym-donkeycar/mysim/rmracerlib")
f = open(reportfile,"a+")
f.write(input_text)
f.close()
cv2.destroyAllWindows()
sys.exit()
