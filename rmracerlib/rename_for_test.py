
import os
import sys
from shutil import copy2

'''
Script to rename photos

Usage: python3 renamefortest.py [args]

If at least 1 argument, will enter custom mode
        Custom mode will ask for 3 inputs: new picture name, source photo directory and count
        Defaults: picture name = "tubbed", source = current directory, count = 0
        New picture name is the string that the new pictures will come from
        Source photo directory is relative directory (dont need to start with "/", start with foldername)
        Count is an integer tail added to the end of the filename
If no arguments given, will use defaults.
    Defaults: picture name = "tubbed", source = current directory, count = 0
'''

def fileWDir(directory, extension):
    return (f for f in os.listdir(directory) if f.endswith('.' + extension))

def main(newName = "tubbed", oldDir = os.getcwd(), count = 0):
    filelist = fileWDir(oldDir, "jpg")

    if not os.path.exists(oldDir + "/newTub"):
        os.makedirs(oldDir + "/newTub")
    
    for filename in filelist:
        
        srcFilename = os.path.join(oldDir, filename)

        dstF = "/newTub/{0} {1}.jpg".format(newName,count)
        dstFilename = oldDir+ dstF
        copy2(srcFilename,dstFilename)
        count+= 1

if __name__ == "__main__":
    # TODO add count start + folder specifier

    if (len(sys.argv)>1):
        newname = input("New picture name: ")
        directory = os.getcwd()+ "/" + input("Directory: ")
        try:
            count = int(input("Start from count: "))
        except:
            count = 0
            print("Number Not Entered: using 0")
        
        if not os.path.exists(directory):
            directory = os.getcwd()
            print("Tub Not Found: using current directory")

        if len(newname) == 0:
            main(oldDir = directory, count = count)
        else:
            main(oldDir = directory, count = count, newName = newname)
    else:
        main()