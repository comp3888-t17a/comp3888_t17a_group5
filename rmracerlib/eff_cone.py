import cv2
import sys
import numpy as np
from src import config as rmcfg
import src.cv.signs as stop
import os
from PIL import Image
from datetime import date

#cv2.namedWindow("live")

directory = "test-data/efficiency_testing/cone"
os.chdir(directory) 

count = 0 
correct = 0
false_positive = 0
false_negative = 0
for filename in os.listdir(os.getcwd()):
    print(filename)
    #frame = Image.open(filename)
    frame = cv2.imread(filename)
    #hsv = cv2.cvtColor(np.float32(frame), rmcfg.COLOUR_CONVERT)
    hsv = cv2.cvtColor(frame, rmcfg.COLOUR_CONVERT)
    frame, result = stop.detect_cone(frame,hsv)
    cv2.imshow("live", frame)
    cv2.waitKey(1)
    while True:
        txt = input("-----------\nEnter 'q' for box present in the right spot;\nEnter 'w' for box present but wrong spot;\nEnter 'e' for box present in correct spot and incorrect spot (more than 1 box);\nEnter 'r' for no box present where it should be;\nEnter 't' if no box is present at all and there shouldn't be one: ").lower()
        if txt == "q":
            count += 1
            correct += 1
            break
        elif txt == "w":
            count += 1
            false_positive += 1
            break
        elif txt == "e":
            count += 1
            false_positive += 1
            correct += 1
            break
        elif txt == "r":
            count += 1
            false_negative += 1
            break
        elif txt == "t":
            #count += 1
            break
        else:
            print("-----------\nERROR: Incorrect input, please try again")
correct_rate = round((correct/count * 100),2)
false_positive_rate = round((false_positive/count * 100),2)
false_negative_rate = round((false_negative/count * 100),2)
results = ("Correct Accuracy %: " + str(correct_rate) + "\nFalse Positive %: " + str(false_positive_rate) + "\nFalse Negative %: " + str(false_negative_rate))
print("Correct Accuracy %: " + str(correct_rate) + "\nFalse Positive %: " + str(false_positive_rate) + "\nFalse Negative %: " + str(false_negative_rate))
user = input("Please enter your name for logging purposes: ")
current_date = date.today()
formatted_date = current_date.strftime("%B %d, %Y")
input_text = ("---These results were recorded on " + str(formatted_date) + " by " + user + "---\n" + results + "\n\n")

os.chdir("C:/Users/Patrick Delaney/Desktop/projects/gym-donkeycar/mysim/rmracerlib")
f = open("cone_efficiency.txt","a+")
f.write(input_text)
f.close()
cv2.destroyAllWindows()
sys.exit()
