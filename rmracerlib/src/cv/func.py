'''
# Functions
'''

import cv2
import numpy as np
import platform

import time
import sys

from src import config as cfg

def create_trackbars(frame):
    # convert to HSV image
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    l_h = cv2.getTrackbarPos("L-H", "trackbars")
    l_s = cv2.getTrackbarPos("L-S", "trackbars")
    l_v = cv2.getTrackbarPos("L-V", "trackbars")
    u_h = cv2.getTrackbarPos("U-H", "trackbars")
    u_s = cv2.getTrackbarPos("U-S", "trackbars")
    u_v = cv2.getTrackbarPos("U-V", "trackbars")

    lower_trackbars = np.array([l_h, l_s, l_v])
    upper_trackbars = np.array([u_s, u_h, u_v])

    mask_tbars1 = cv2.inRange(hsv, lower_trackbars, upper_trackbars)

    # using morphology to test for end result
    mask_tbars = cv2.morphologyEx(mask_tbars, cv2.MORPH_OPEN, kernel, iterations=1)
    mask_tbars = cv2.morphologyEx(mask_tbars, cv2.MORPH_CLOSE, kernel, iterations=1)
    mask_tbars = cv2.dilate(mask_tbars, kernel, iterations=2)

    trackview = cv2.bitwise_and(frame,frame,mask = mask_tbars)
    cv2.imshow("trackbarview", trackview)

def contours_detection(mask, frame):
    contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for cnt in contours:
        area = cv2.contourArea(cnt)
        approx = cv2.approxPolyDP(cnt, 0.01 * cv2.arcLength(cnt, True), True)

        if area > AREA_SIZE:

            if len(cnt) == 8:
                cv2.drawContours(frame, [approx], 0, (0,0,0), 5)
                x = approx.ravel()[0]
                y = approx.ravel()[1]
                cv2.putText(frame, "STOP", (x,y), font, 1, (0,0,0))
                return "stop"
    return None


"""
###  HELPER FUNCTIONS
"""


def valid_range(x, y, w, h, frame):
    '''
    This function returns if an roi is in a valid or acceptable part of the image.  The reason
     for having this is due to extra parts of the frame containing reflections.
    '''

    height, width = frame.shape[:2]

    lw = 0      # furthest left width
    rw = width   # furthest right width

    cw = False
    ch = False

    if (cfg.ROOF < y < cfg.HORIZON) and (cfg.ROOF < y+h < cfg.HORIZON):
        ch = True
    if (lw < x < rw) and (lw < x+w < rw):
        cw = True

    if ch and cw:
        return True
    else:
        return False

def valid_range_bottom(x, y, w, h, frame):
    '''
    This function returns if an roi is in a valid or acceptable part of the image.  The reason
     for having this is due to extra parts of the frame containing reflections.
    '''

    height, width = frame.shape[:2]

    ch = False
    mid = (cfg.HORIZON + cfg.ROOF)/2
    if (mid < y < height) and (mid < y+h < height):
        ch = True
        
    return ch

def is_squarish(height, width):
    # calculate ratio of sides - anything not square is not worth checking
    a = height / width
    b = width / height

    if (0.5 < a < 2.0) and (0.5 < b < 2.0):
        return True
    else:
        return False

def sign_direction(img):
  """ Reads in a ROI and outputs either: right, left or None """
  # sharpen the ROI so it is clearer for detection
  sharpen = cv2.GaussianBlur(img, (3,3), 3)
  sharpen = cv2.addWeighted(img, 2, sharpen, -1, 0)

  # convert image to binary
  grey = cv2.cvtColor(sharpen, cv2.COLOR_BGR2GRAY)
  thresh, binary = cv2.threshold(grey, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)


  # get picture shape information for selecting a smaller ROI
  height, width = binary.shape[:2]
  total = height * width

  # calculate ratios
  n_white_pix = np.sum(binary == 255)
  w_ratio = int(n_white_pix / total * 100)
  n_black_pix = np.sum(binary == 0)
  b_ratio = int(n_black_pix / total * 100)

  # check
  if ( ( 35 <= w_ratio <= 65 ) and ( 35 <= b_ratio <= 65 ) ):
    # run the sign detection algorithm
    result = direction_check(binary)

    if result is not None:
      #print(result, " sign")
      return result

  # if we fail any tests, return None
  return None

def direction_check(binary):
  """ Checks the sign direction based on relevant information in the image """
  # extract image information
  height, width = binary.shape[:2]

  # set up our regions at 25%, 50% and 75% marks
  #  we are only going to look at the center of the binary image
  h1 = int(height/4)  # was 0
  h2 = int(height/2)
  h3 = int(h1+h2+1)     # was height

  v1 = int(width/4)   # was 0
  v2 = int(width/2)
  v3 = int(v1+v2+1)     # was width

  # quadrants / regions
  q1Block = binary[h1:h2, v2:v3]
  q2Block = binary[h1:h2, v1:v2]
  q3Block = binary[h2:h3, v1:v2]
  q4Block = binary[h2:h3, v2:v3]

  # add up the number of white pixels in each quadrant.
  q1Sum = np.sum(q1Block == 255)
  q2Sum = np.sum(q2Block == 255)
  q3Sum = np.sum(q3Block == 255)
  q4Sum = np.sum(q4Block == 255)


  # information search - check which region has the most white pixels and then
  #  determine if the sign is left or right.
  if q4Sum > q3Sum: #and q1Sum < q2Sum:
    return "left"
  elif q4Sum < q3Sum: #and q1Sum > q2Sum:
    return "right"
  else:
    return None

def pentagon_sign_direction(img):
    """ Reads in a ROI and outputs either: right, left or None """
    # sharpen the ROI so it is clearer for detection
    sharpen = cv2.GaussianBlur(img, (3,3), 3)
    sharpen = cv2.addWeighted(img, 1.5, sharpen, -0.5, 0)
    # convert image to binary
    grey = cv2.cvtColor(sharpen, cv2.COLOR_BGR2GRAY)
    thresh,binary  = cv2.threshold(grey, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

    # get picture shape information for selecting a smaller ROI
    height, width = binary.shape[:2]

    # CHECK 2 - check the mix of white and black pixels to eliminate false detections
    # calculate total number of pixels 
    total = height * width

    # calculate ratios
    n_white_pix = np.sum(binary == 255)
    w_ratio = int(n_white_pix / total * 100)
    n_black_pix = np.sum(binary == 0)
    b_ratio = int(n_black_pix / total * 100)

    return pentagon_direction_check(binary)
    
def pentagon_direction_check(binary):
    """ Checks the sign direction based on relevant information in the image """
    # extract image information
    height, width = binary.shape[:2]
    #print(binary)

    #  we are only going to look at the center of the binary image
    h1 = int(height/4)  # was 0
    h2 = int(height/2)
    h3 = int(h1+h2+1)     # was height

    v1 = int(width/4)   # was 0
    v2 = int(width/2)
    v3 = int(v1+v2+1)     # was width

    # quadrants / regions
    q1Block = binary[0:h2, v2+1:width] # top right block
    q2Block = binary[0:h2, 0:v2] # top left block
    q3Block = binary[h2+1:height, 0:v2] # bottom left block
    q4Block = binary[h2+1:height, v2+1:width] # bottom right block

    # add up the number of white pixels in each quadrant.
    q1Sum = np.sum(q1Block == 255)
    q2Sum = np.sum(q2Block == 255)
    q3Sum = np.sum(q3Block == 255)
    q4Sum = np.sum(q4Block == 255)

    # print(q1Sum, q2Sum, q3Sum, q4Sum)

    # information search - check which region has the most white pixels and then
    #  determine if the sign is left or right.
    if q3Sum + q2Sum > q1Sum + q4Sum:
        return "left"
    elif q1Sum + q4Sum > q3Sum + q2Sum:
        return "right"
    else:
        return None   
