#!/usr/bin/env python3
"""
Scripts for operating the OpenCV by Robotics Masters
 with the Donkeycar
author: @wallarug (Cian Byrne) 2020
contrib: @peterpanstechland 2020

Note: To be used with code.py bundled in this repo. See donkeycar/contrib/robohat/code.py
"""

import time
import donkeycar as dk
import cv2

from src.cv.lights import detect_traffic
from src.cv.signs import detect_stop, detect_turn, detect_park, detect_cone
from src.datastructure.Queue10 import Queue10
from src import config as rmcfg

class RMRacerCV:

    def __init__(self, cfg, debug=False):
        self.throttle = 0
        self.steering = 0
        self.brake = 0
        self.debug = debug
        self.speed = 0

        # actions for the car & error detection
        self.stop_sign = Queue10(rmcfg.DK_COUNTER_SIZE)
        self.right_sign = Queue10(rmcfg.DK_COUNTER_SIZE)
        self.left_sign = Queue10(rmcfg.DK_COUNTER_SIZE)
        self.park_sign = Queue10(rmcfg.DK_COUNTER_SIZE)
        self.red_traf = Queue10(rmcfg.DK_COUNTER_SIZE)
        self.grn_traf = Queue10(rmcfg.DK_COUNTER_SIZE)
        self.cone_sign = Queue10(rmcfg.DK_COUNTER_SIZE)
        
        """
        if we are already running a sequence (like parking) do
        not do anything except finish that sequence
        """
        self.running = False
        self.action = None
        self.wait = 0

    def update(self):
        pass

    def run_threaded(self, img_arr, throttle, steering, brake, speed, debug=False):
        return self.run(img_arr, throttle, steering, brake, speed)

    def shutdown(self):
        try:
            pass
        except:
            pass

    def run(self, img_arr, throttle, steering, brake, speed, debug=False):
        if throttle == None:
            throttle = 0
        if img_arr is None:
            return img_arr, throttle, steering, brake
        if self.running == False:
            ## pre-process the image to save time...
            bgr = cv2.cvtColor(img_arr, cv2.COLOR_BGR2RGB) 
            hsv = cv2.cvtColor(bgr, rmcfg.COLOUR_CONVERT)
            if rmcfg.DEMO_MODE:
                cv2.circle(img_arr, (3,3), 2, (0,255,0), 3)
            
            """
            ##Detection Code
            This portion of the code houses the detection
            """
            
            # STOP SIGN DETECTION

            stop = detect_stop(img_arr, hsv)
            if stop[1] == True:
                # add one to counter, check if above threshold
                if rmcfg.DK_SHOW_TEXT_DEBUG:
                    print("detection - stop")
                if self.stop_sign.put(1) > rmcfg.DK_COUNTER_THRESHOLD and throttle != None:
                    self.running = True
                    if rmcfg.DK_SHOW_TEXT_DEBUG: print("stopping")
                    self.action = "stopping"
                return img_arr, throttle, steering, brake
            else:
                self.stop_sign.put()

            # TURN SIGN DETECTION

            turn = detect_turn(img_arr, hsv)
            if turn[1] == "right":
                if self.right_sign.put(1) > rmcfg.DK_COUNTER_THRESHOLD:
                    self.running = True
                    self.action = "right"
                if rmcfg.DK_SHOW_TEXT_DEBUG: print("detection - right turn")
                return img_arr, throttle, steering, brake
            elif turn[1] == "left":
                if self.left_sign.put(1) > rmcfg.DK_COUNTER_THRESHOLD:
                    self.running = False
                    self.action = "left"
                if rmcfg.DK_SHOW_TEXT_DEBUG: print("detection - left turn")
                return img_arr, throttle, steering, brake
            else:
                self.right_sign.put()
                self.left_sign.put()

            # TRAFFIC LIGHT DETECTION

            traffic_light = detect_traffic(img_arr, hsv)
            if traffic_light[1] == "red":
                if rmcfg.DK_SHOW_TEXT_DEBUG: print("detection - red light")
                return img_arr, throttle, steering, brake
            elif traffic_light[1] == "green":
                if rmcfg.DK_SHOW_TEXT_DEBUG: print("detection - green light")
                return img_arr, throttle, steering, brake
            elif traffic_light[1] == "amber":
                if rmcfg.DK_SHOW_TEXT_DEBUG: print("detection - amber light")
                return img_arr, throttle, steering, brake

            # PARK SIGN DETECTION

            park = detect_park(img_arr, hsv)
            if park[1] == True:
                if rmcfg.DK_SHOW_TEXT_DEBUG: print("detection - park")
                if self.park_sign.put(1) > rmcfg.DK_COUNTER_THRESHOLD:
                    self.running = True
                    self.action = "parking"
                    if rmcfg.DK_SHOW_TEXT_DEBUG: print("preparing to park")
                return img_arr, throttle, steering, brake
            else:
                self.park_sign.put()

            # TRAFFIC CONE DETECTION
            cone = detect_cone(img_arr, hsv)
            if cone[1] != None:
                if cone[1] != "not_in_path":
                    if rmcfg.DK_SHOW_TEXT_DEBUG: print("detection - cone {0}".format(cone[1]))
                    if self.cone_sign.put(1) > rmcfg.DK_COUNTER_THRESHOLD:
                        self.running = True
                        self.action = 'cone {0}'.format(cone[1])

                return img_arr, throttle, steering, brake


        """
        ##Response Code
        This portion of the code pertains to the responses based on the detection code results above
        Future improvements: making size of sign proportional to time, accounting for speed (esp in turn and swerve function)
        """
        if self.running == True:

            #STOP SIGN RESPONSE

            if self.action == "stopping":
                if self.wait <= rmcfg.DK_STOP_DELAY:  # wait before stopping
                    self.wait += 1
                    if rmcfg.DK_SHOW_TEXT_DEBUG: print("action - preparing to stop")
                    return img_arr, throttle, steering, brake
                elif self.wait <= rmcfg.DK_STOP_RUNTIME:  # execute, action is running
                    if rmcfg.DK_SHOW_TEXT_DEBUG: print("action - stopping {0}".format(self.wait))
                    self.wait += 1
                    return img_arr, 0, steering, rmcfg.DK_HARD_BRAKE
                elif self.wait <= rmcfg.DK_STOP_TOTALTIME: # wait at the stop light
                    self.wait += 1
                    if (self.wait - rmcfg.DK_STOP_TOTALTIME%20 == 0 and rmcfg.DK_SHOW_TEXT_DEBUG): print ("stopped waiting - {0} seconds".format(self.wait%20))
                    return img_arr, 0, steering, rmcfg.DK_HARD_BRAKE
                elif self.wait > rmcfg.DK_STOP_TOTALTIME:  # complete, leave action
                    self.wait = 0
                    self.action = None
                    self.running = False
                    self.stop_sign.clear()
                    if rmcfg.DK_SHOW_TEXT_DEBUG: print("complete! control returned!")
                    return img_arr, throttle, steering, rmcfg.DK_RELEASE_BRAKE

             #TRAFFIC LIGHT RESPONSE

            elif self.action == "light_stop":
                if self.wait <= rmcfg.DK_ACTION_DELAY: # waiting to stop at lights
                    self.wait += 1
                    if rmcfg.DK_SHOW_TEXT_DEBUG: print("action - preparing to stop at lights")
                    return img_arr, steering, steering, brake
                else: # continue if green else stop if red or amber
                    bgr = cv2.cvtColor(img_arr, cv2.COLOR_BGR2RGB)
                    hsv = cv2.cvtColor(bgr, rmcfg.COLOUR_CONVERT)
                    traffic = detect_traffic(img_arr, hsv)

                    if traffic[1] == "green":
                        self.grn_traf.put(1)
                        if self.grn_traf.total > rmcfg.DK_COUNTER_THRESHOLD:
                            if rmcfg.DK_SHOW_TEXT_DEBUG: print("detect light green - go")
                            self.wait = 0
                            self.action = None
                            self.running = False
                            self.red_traf.clear()
                            return img_arr, throttle, steering, rmcfg.DK_RELEASE_BRAKE
                    elif traffic[1] == "red" or traffic[1] == "amber": # stop on red/amber
                        return img_arr, throttle, steering, rmcfg.DK_HARD_BRAKE
                    else: # doesnt detect traffic light at all will continue
                        if rmcfg.DK_SHOW_TEXT_DEBUG: print("lost traffic light - go")
                        self.wait = 0
                        self.action = None
                        self.running = False
                        self.red_traf.clear()
                        return img_arr, throttle, steering, rmcfg.DK_RELEASE_BRAKE

            #CONE RESPONSE

            elif  "cone" in self.action:
                coneSide = self.action.split(" ")[1]
                phaseModifier = 1 # used to mirror turn direction after dodge before straightening
                if (rmcfg.DK_CONE_DODGE_TIME < self.wait < DK_CONE_CORRECT_TIME):
                    phaseModifier = -1
                
                if (coneSide == "right" ):
                    tempSteer = - phaseModifier * rmcfg.DK_CONE_STEER
                    # +ve steering is right, -ve is left
                else:
                    tempSteer = + phaseModifier * rmcfg.DK_CONE_STEER

                # dodge cone
                if (self.wait < rmcfg.DK_CONE_DODGE_TIME):
                    if rmcfg.DK_SHOW_TEXT_DEBUG: print("swerving {0}".format(coneSide))
                    self.wait+= 1
                    #correct lane position
                    return img_arr, throttle, tempSteer, brake
                elif (self.wait < rmcfg.DK_CONE_CORRECT_TIME):
                    if rmcfg.DK_SHOW_TEXT_DEBUG: print("return position {0}".format(coneSide))
                    self.wait+= 1
                    #Straighten car
                    return img_arr, throttle, tempSteer, brake
                elif (self.wait < rmcfg.DK_CONE_TOTAL):
                    if rmcfg.DK_SHOW_TEXT_DEBUG: print("straightening {0}".format(coneSide))
                    self.wait+= 1
                    #return control of car
                    return img_arr, throttle, tempSteer, brake
                else:
                    if rmcfg.DK_SHOW_TEXT_DEBUG: print("cone dodged - releasing controls")
                    self.wait = 0
                    self.action = None
                    self.running = False
                    self.cone_sign.clear()
                    return img_arr, throttle, tempSteer, brake

             # PARK RESPONSE
             #    
            elif self.action == "parking":
                if self.wait <= rmcfg.DK_STOP_DELAY: #arrive at sign before stopping
                    self.wait += 1
                    if rmcfg.DK_SHOW_TEXT_DEBUG: print("action - preparing to park (stop)")
                    return img_arr, throttle, steering, brake
                elif self. wait <= rmcfg.DK_STOP_RUNTIME: # action brake
                    if rmcfg.DK_SHOW_TEXT_DEBUG: print("action - parking (stopping)")
                    self.wait += 1
                    return img_arr, 0, steering, rmcfg.DK_HARD_BRAKE
                elif self.wait <= rmcfg.DK_STOP_TOTALTIME: # hang out for 3 seconds before starting again
                    self.wait += 1
                    return img_arr, 0, steering, rmcfg.DK_HARD_BRAKE
                elif self.wait > rmcfg.DK_STOP_TOTALTIME:
                    self.wait = 0
                    self.action = None
                    self.running = False
                    self.park_sign.clear()
                    return img_arr, throttle, steering, rmcfg.DK_RELEASE_BRAKE

            # TURN SIGN RESPONSE

            elif (self.action == "left" or self.action == "right") and self.wait <= rmcfg.DK_ACTION_DELAY:
                if self.wait == 0:
                    self.steering = steering
                self.wait += 1
                if rmcfg.DK_SHOW_TEXT_DEBUG: print("action - turning")
                return img_arr, throttle, self.steering, brake
                # pre calculate steering and calculate it only once
            elif (self.action == "left" or self.action == "right") and self.wait > rmcfg.DK_ACTION_DELAY:
                if self.action == "left" and self.wait < (rmcfg.DK_ACTION_DELAY + rmcfg.DK_ACTION_RUNTIME):
                    self.wait += 1
                    if rmcfg.DK_SHOW_TEXT_DEBUG: print("action - left")
                    return img_arr, throttle, self.steering - 1.0, brake
                elif self.action == "right" and self.wait < (rmcfg.DK_ACTION_DELAY + rmcfg.DK_ACTION_RUNTIME):
                    self.wait += 1
                    if rmcfg.DK_SHOW_TEXT_DEBUG: print("action - right")
                    return img_arr, throttle, self.steering + 1.0, brake
                else:
                    #reset steering
                    self.wait = 0
                    self.action = None
                    self.running = False
                    self.left_sign.clear()
                    self.right_sign.clear()
                    if rmcfg.DK_SHOW_TEXT_DEBUG: print("complete! control returned!")
                    return img_arr, throttle, self.steering, brake
