# -*- coding: utf-8 -*-

"""Top-level package for OpenAI Gym Environments for Donkey Car."""

__author__ = """Tawn Kramer"""
__email__ = 'tawnkramer@gmail.com'
__version__ = '1.0.16'

from gym.envs.registration import register
from .envs.donkey_env import GeneratedRoadsEnv, WarehouseEnv, AvcSparkfunEnv, GeneratedTrackEnv, MountainTrackEnv, Track2Env, Track1Env, RacingLeagueEnv

register(
    id='donkey-generated-roads-v0',
    entry_point='gym_donkeycar.envs.donkey_env:GeneratedRoadsEnv',
)

register(
    id='donkey-warehouse-v0',
    entry_point='gym_donkeycar.envs.donkey_env:WarehouseEnv',
)

register(
    id='donkey-avc-sparkfun-v0',
    entry_point='gym_donkeycar.envs.donkey_env:AvcSparkfunEnv',
)

register(
    id='donkey-generated-track-v0',
    entry_point='gym_donkeycar.envs.donkey_env:GeneratedTrackEnv',
)

register(
    id='donkey-mountain-track-v0',
    entry_point='gym_donkeycar.envs.donkey_env:MountainTrackEnv',
)

register(
    id='donkey-track2-v0',
    entry_point='gym_donkeycar.envs.donkey_env:Track2Env',
)

register(
    id='donkey-track1-v0',
    entry_point='gym_donkeycar.envs.donkey_env:Track1Env',
)

register(
    id='donkey-racing-league-v0',
    entry_point='gym_donkeycar.envs.donkey_env:RacingLeagueEnv',
)

register(
    id='donkey-hockenheim-v0',
    entry_point='gym_donkeycar.envs.donkey_env:HockenheimEnv',
)

register(
    id='donkey-monaco-v0',
    entry_point='gym_donkeycar.envs.donkey_env:MonacoEnv',
)